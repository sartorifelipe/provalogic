package provalogic;

public class Transportadora {

    String nome = "";
    double valorAereo = 0.0;
    double valorTerrestre = 0.0;
    double tempoTerrestre = 0.0;
    double tempoAereo = 0.0;
    String tipoTransporte = "";

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getValorAereo() {
        return valorAereo;
    }

    public void setValorAereo(double valorAereo) {
        this.valorAereo = valorAereo;
    }

    public double getValorTerrestre() {
        return valorTerrestre;
    }

    public void setValorTerrestre(double valorTerrestre) {
        this.valorTerrestre = valorTerrestre;
    }

    public double getTempoTerrestre() {
        return tempoTerrestre;
    }

    public void setTempoTerrestre(double tempoTerrestre) {
        this.tempoTerrestre = tempoTerrestre;
    }

    public double getTempoAereo() {
        return tempoAereo;
    }

    public void setTempoAereo(double tempoAereo) {
        this.tempoAereo = tempoAereo;
    }

    public String getTipoTransporte() {
        return tipoTransporte;
    }

    public void setTipoTransporte(String tipoTransporte) {
        this.tipoTransporte = tipoTransporte;
    }
}
