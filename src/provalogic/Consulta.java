package provalogic;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import provalogic.gui.MainScreenGUI;
import provalogic.obj.DadosConsulta;

public class Consulta {

    double valorTotal = 99999999;
    String nomeTransportadoraValor = "";
    String nomeTransportadoraTempo = "";
    double tempo = 9999999;
    
    public void consulta(DadosConsulta oDados) {
        Global global = new Global();
        DecimalFormat df = new DecimalFormat("###,##0.00");

        Map<String, Double> mapValor = new HashMap<String, Double>();
        Map<String, Double> mapTempo = new HashMap<String, Double>();

        Transportadora t1 = new Transportadora();
        t1.setNome("Transportadora 1");
        t1.setTipoTransporte("Terrestre");
        t1.setTempoTerrestre(60);
        t1.setValorTerrestre(50);

        Transportadora t2 = new Transportadora();
        t2.setNome("Transportadora 2");
        t2.setTipoTransporte("Aereo/Terrestre");
        t2.setTempoAereo(30);
        t2.setValorAereo(200);
        t2.setTempoTerrestre(59);
        t2.setValorTerrestre(75);

        Transportadora t3 = new Transportadora();
        t3.setNome("Transportadora 3");
        t3.setTipoTransporte("Aereo/Terrestre");
        t3.setTempoAereo(33);
        t3.setValorAereo(180);
        t3.setTempoTerrestre(65);
        t3.setValorTerrestre(55);

        Transportadora t4 = new Transportadora();
        t4.setNome("Transportadora 4");
        t4.setTipoTransporte("Aereo");
        t4.setTempoAereo(30);
        t4.setValorAereo(175);

        switch (oDados.getTipoTransporte()) {
            case 0:
                mapValor.put(t1.getNome(), (t1.getValorTerrestre() * oDados.getDistancia()) / 10);
                mapTempo.put(t1.getNome(), (t1.getTempoTerrestre() / 60) * oDados.getDistancia());
                mapValor.put(t2.getNome(), (t2.getValorTerrestre() * oDados.getDistancia()) / 10);
                mapTempo.put(t2.getNome(), (t2.getTempoTerrestre() / 60) * oDados.getDistancia());
                mapValor.put(t3.getNome(), (t3.getValorTerrestre() * oDados.getDistancia()) / 10);
                mapTempo.put(t3.getNome(), (t3.getTempoTerrestre() / 60) * oDados.getDistancia());
                break;
            case 1:
                mapValor.put(t2.getNome(), (t2.getValorAereo() * oDados.getDistancia()) / 10);
                mapTempo.put(t2.getNome(), (t2.getTempoAereo() / 60) * oDados.getDistancia());
                mapValor.put(t3.getNome(), (t3.getValorAereo() * oDados.getDistancia()) / 10);
                mapTempo.put(t3.getNome(), (t3.getTempoAereo() / 60) * oDados.getDistancia());
                mapValor.put(t4.getNome(), (t4.getValorAereo() * oDados.getDistancia()) / 10);
                mapTempo.put(t4.getNome(), (t4.getTempoAereo() / 60) * oDados.getDistancia());
                break;
            default:
                mapValor.put(t2.getNome(), (t2.getValorAereo() * oDados.getDistancia()) / 10);
                mapTempo.put(t2.getNome(), (t2.getTempoAereo() / 60) * oDados.getDistancia());
                mapValor.put(t3.getNome(), (t3.getValorAereo() * oDados.getDistancia()) / 10);
                mapValor.put(t3.getNome(), (t3.getValorAereo() * oDados.getDistancia()) / 10);
                mapValor.put(t4.getNome(), (t4.getValorAereo() * oDados.getDistancia()) / 10);
                mapTempo.put(t4.getNome(), (t4.getTempoAereo() / 60) * oDados.getDistancia());
                mapValor.put(t1.getNome(), (t1.getValorTerrestre() * oDados.getDistancia()) / 10);
                mapTempo.put(t1.getNome(), (t1.getTempoTerrestre() / 60) * oDados.getDistancia());
                mapValor.put(t2.getNome(), (t2.getValorTerrestre() * oDados.getDistancia()) / 10);
                mapTempo.put(t2.getNome(), (t2.getTempoTerrestre() / 60) * oDados.getDistancia());
                mapValor.put(t3.getNome(), (t3.getValorTerrestre() * oDados.getDistancia()) / 10);
                mapTempo.put(t3.getNome(), (t3.getTempoTerrestre() / 60) * oDados.getDistancia());
                break;
        }
        

        for (Map.Entry<String, Double> entry : mapValor.entrySet()) {
            if (valorTotal > entry.getValue()) {
                valorTotal = entry.getValue();
                nomeTransportadoraValor = entry.getKey();
            }
            
        }
        for (Map.Entry<String, Double> entry : mapTempo.entrySet()) {
            if (tempo > entry.getValue()) {
                tempo = entry.getValue();
                nomeTransportadoraTempo = entry.getKey();
            }
            
        }
        if(oDados.getTipoPrioridade() == 0){
            MainScreenGUI.nomeTransportadora = nomeTransportadoraValor;
            MainScreenGUI.valor = valorTotal;
        } else{
            MainScreenGUI.nomeTransportadora = nomeTransportadoraTempo;
            MainScreenGUI.tempo = tempo;
        }
        
        
    }
}
