package provalogic;

public enum TipoTransporte {

    AEREO(0, "Aéreo"),
    TERRESTRE(1, "Terrestre");

    String descricao = "";
    int id = -1;

    TipoTransporte(int id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public int getId() {
        return this.id;
    }
}
