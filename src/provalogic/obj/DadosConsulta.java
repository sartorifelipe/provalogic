package provalogic.obj;

public class DadosConsulta {
    
    private String origem = "";
    private String destino = "";
    private double distancia = 0;
    private int TipoTransporte = -1;
    private int TipoPrioridade = -1;

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public int getTipoTransporte() {
        return TipoTransporte;
    }

    public void setTipoTransporte(int TipoTransporte) {
        this.TipoTransporte = TipoTransporte;
    }

    public int getTipoPrioridade() {
        return TipoPrioridade;
    }

    public void setTipoPrioridade(int TipoPrioridade) {
        this.TipoPrioridade = TipoPrioridade;
    }
    
    
}
